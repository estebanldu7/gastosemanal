import React, { Component } from 'react';
import '../css/App.css';
import Header from '../components/Header'
import Formulario from '../components/Form'
import Listed from "./Listed";
import {validarPresupuesto} from "../helper";
import ControlPresupuesto from "./ControlPresupuesto";

class App extends Component {

  //create a state
  state={
    presupuesto: '',
    restante: '',
    gastos: {}
  };

  componentDidMount(){
      this.obtenerPresupuesto();
  }

  obtenerPresupuesto = () => {
    //show dialog to load page
    let presupuesto = prompt('Cual es el presupuesto?');
    let resultado = validarPresupuesto(presupuesto)

    if(resultado){
        this.setState(() => {
            return {presupuesto : presupuesto, restante: presupuesto}
        });
    }else{
      this.obtenerPresupuesto();
    }
  }

  agregarGasto = gasto => {
    const gastos = {...this.state.gastos};

    gastos[`gasto${Date.now()}`] = gasto;
    this.restarPresupuesto(gasto.cantidadGasto)

    this.setState(() => {
      return {gastos : gastos}
    });
  };

    restarPresupuesto = cantidad => {
        let restar = Number(cantidad);
        let restante = this.state.restante;
        restante -= restar;

        this.setState(() => {
            return {restante : restante}
        });
    }

  render() {
    return (
      <div className="App container">
        <Header title='Gasto semanal'/>

        <div className="contenido-principal contenido">
          <div className="row">
            <div className="one-half column">
              <Formulario  agregarGasto={this.agregarGasto}/>
            </div>
            <div className="one-half column">
              <Listed gastos={this.state.gastos}/>

              <ControlPresupuesto
                  presupuesto={this.state.presupuesto}
                  restante={this.state.restante}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
