import React from 'react';
import Presupuesto from "./Presupuesto";
import Rest from "./Rest";

class ControlPresupuesto extends React.Component{
    render(){
        return(
           <React.Fragment>
               <Presupuesto
                    presupuesto={this.props.presupuesto}
               />
               <Rest
                    presupuesto={this.props.presupuesto}
                    restante={this.props.restante}
               />
           </React.Fragment>
        )
    }
}

export default ControlPresupuesto;

