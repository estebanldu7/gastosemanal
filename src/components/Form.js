import React from 'react';

class FormularioFasto extends React.Component{

    //Read data
    nombreGasto=React.createRef();
    cantidadGasto=React.createRef();

    crearGasto = (e) => {
        //Prevent default
        e.preventDefault();
        //create object
        const gasto= {
            nombreGasto: this.nombreGasto.current.value,
            cantidadGasto: this.cantidadGasto.current.value
        }

        //add and send for props
        this.props.agregarGasto(gasto)


    };

    render(){
        return(
            <form onSubmit={this.crearGasto}>
                <h2>Agrega tus gastos aqui</h2>
                <div className="campo">
                    <label>Nombre Gasto</label>
                    <input className="u-full-width" ref={this.nombreGasto} type="text" placeholder="Ej. Transporte" />
                </div>

                <div className="campo">
                    <label>Cantidad</label>
                    <input className="u-full-width" ref={this.cantidadGasto} type="text" placeholder="Ej. 300" />
                </div>

                <input className="button-primary u-full-width" type="submit" value="Agregar" />
            </form>
        )
    }
}

export default FormularioFasto;