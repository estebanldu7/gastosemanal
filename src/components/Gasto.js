import React from 'react';

class Gasto extends React.Component{
    render(){
        const {nombreGasto, cantidadGasto} = this.props.gasto

        return(
           <li className="gastos">
                <p>
                    {nombreGasto}
                    <span className="gasto">
                        $ {cantidadGasto}
                    </span>
                </p>
           </li>
        )
    }
}


export default Gasto;